#!/usr/bin/make -f

#export DH_VERBOSE=1

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
ifneq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
CROSS= --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
else
CROSS= --build $(DEB_BUILD_GNU_TYPE)
endif

export CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
export CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
export LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)

config-stamp:
	dh_testdir
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	cp -f /usr/share/misc/config.sub config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	cp -f /usr/share/misc/config.guess config.guess
endif
	./configure $(CROSS) --prefix=/usr --mandir=\$${prefix}/share/man --infodir=\$${prefix}/share/info CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)"
	touch config-stamp

configure: config-stamp

build: build-stamp

build-stamp: build-arch-stamp build-indep-stamp
	touch build-stamp

build-arch: build-arch-stamp
build-arch-stamp:  configure
	dh_testdir
	$(MAKE)
	touch $@

build-indep: build-indep-stamp
build-indep-stamp: configure
	dh_testdir
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f config-stamp build-indep-stamp build-arch-stamp build-stamp

	[ ! -f Makefile ] || $(MAKE) clean
	-rm -rf .pc
	rm -f config.sub config.guess config.status Makefile

	dh_clean

install: build-stamp
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) prefix=$(CURDIR)/debian/xplot-xplot.org/usr install


binary-indep: build-stamp install

binary-arch: install
	dh_testdir
	dh_testroot
	dh_installchangelogs
	dh_installdocs
	dh_installexamples
	dh_installman
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
